/* ===== Navbar Search ===== */
$(function(){
    $('#navbar-search > a').on('click', function() {
        $('#navbar-search > a > i').toggleClass('fa-search fa-times');
        $("#navbar-search-box").toggleClass('show hidden animated fadeInUp');
        return false;
    });
    $('#bonus .pricing-number > .fa-scissors').on('click', function() {
        $(this).css('left', '100%');    /* Cutting */
        setTimeout(function(){          /* Removing the scissors */
            $('#bonus .pricing-number > .fa-scissors').addClass('hidden');
            $('#bonus .pricing-body ul').addClass('animated fadeOutDown');
        }, 2000);
        return false;
    });

    /* ===== Lost password form ===== */

    $('.pwd-lost > .pwd-lost-q > a').on('click', function() {
        $(".pwd-lost > .pwd-lost-q").toggleClass("show hidden");
        $(".pwd-lost > .pwd-lost-f").toggleClass("hidden show animated fadeIn");
        return false;
    });

    $('.rating .voteup').on('click', function () {
        var pcid=$(this).attr("data-id");
        if(CookieHandle.getCookie("rating_vate"+pcid)!=null){
            $(this).addClass("clicked"); return false;
        }
        var up = $(this).closest('div').find('.up'),number=parseInt(up.text(),10);
        $.ajax({
            type:"post",
            data:{"action":"up","pcid":pcid},
            dataType:"json",
            url:"/tcms/posts/vote",
            success:function(data){
                CookieHandle.addCookie("rating_vate"+pcid,"1",24);
                up.text(parseInt(up.text(),10) + 1);
            }
        });
        return false;
    });
    $('.rating .votedown').on('click', function () {
        if(CookieHandle.getCookie("rating_vate")!=null){
            $(this).addClass("clicked"); return false;
        }
        var down = $(this).closest('div').find('.down');
        down.text(parseInt(down.text(),10) + 1);
        return false;
    });

    /* ===== Responsive Showcase ===== */

    $('.responsive-showcase ul > li > i').on('click', function() {
        var device = $(this).data('device');
        $('.responsive-showcase ul > li > i').addClass("inactive");
        $(this).removeClass("inactive");
        $('.responsive-showcase img').removeClass("show");
        $('.responsive-showcase img').addClass("hidden");
        $('.responsive-showcase img' + device).toggleClass("hidden show");
        $('.responsive-showcase img' + device).addClass("animated fadeIn");
        return false;
    });

    $(".email-subscribe").click(function(){
        var email =$.trim($("#email").val());
        if(email.lenght==0) return false;
        $.ajax({
            type:"post",
            dataType:"json",
            data:{"email":email},
            url:"https://www.tpframe.com/taskmger/email_subscribe",
            success:function(data){
                $("#msgTip").show().html(data.msg).fadeOut(1000);
            }
        });
    });
});

var CookieHandle={
    addCookie:function(name,value,expireHours){
        var cookieString=name+"="+escape(value)+"; path=/";
        //判断是否设置过期时间
        if(expireHours>0){
            var date=new Date();
            date.setTime(date.getTime()+expireHours*3600*1000);
            cookieString=cookieString+";expires="+date.toGMTString();
        }
        document.cookie=cookieString;
    },
    getCookie:function(name){
        var strcookie=document.cookie;
        var arrcookie=strcookie.split("; ");
        for(var i=0;i<arrcookie.length;i++){
            var arr=arrcookie[i].split("=");
            if(arr[0]==name)return unescape(arr[1]);
        }
        return null;
    },
    delCookie:function(name){//删除cookie
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval=getCookie(name);
        if(cval!=null) document.cookie= name + "="+cval+"; path=/;expires="+exp.toGMTString();
    }
}
