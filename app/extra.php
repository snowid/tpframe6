<?php
// +----------------------------------------------------------------------
// | extra配置加载
// +----------------------------------------------------------------------
$extra = ['version'];

$arr = [];

foreach ($extra as $key => $value) {

	$file = dirname(__FILE__).DIRECTORY_SEPARATOR."extra".DIRECTORY_SEPARATOR.$value.".php";

	if(file_exists($file)) $arr=array_merge($arr,include($file));

}

return $arr;