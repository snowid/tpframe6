<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\logic;
use \tpfcore\Core;
use \tpfcore\util\Config;
/**
 *  设置逻辑
 */
class Setting extends AdminBase
{
	public function clearRuntime(){
		\think\facade\Cache::clear();		//清空cache
		$runtimepath = dirname(app()->getRuntimePath());

		$dir_arr = scandir($runtimepath);
		foreach ($dir_arr as $key => $value) {
			if(!(strpos($value,'.') !== false) && $value!="session"){
				$this->delDirAndFile($runtimepath.DIRECTORY_SEPARATOR.$value);
			}
		}
        return [RESULT_SUCCESS, '缓存已更新成功', url('backend/Index/main')];
	}
	public function  editSetting($data){
		foreach ($data['configs'] as $key => $value) {
			$update_config[$key]=$value;
		}

		unset($data['configs']);

		Config::updateConfig(APP_PATH."extra/config.php",$update_config);
	
		$result=Core::loadModel($this->name)->saveObject($data);
		if($result){
			return [RESULT_SUCCESS, '更新成功', url('Setting/site')];
		}else{
			return [RESULT_SUCCESS, '更新成功', url('Setting/site')];
		}
	}
	public function editMail($data){
		$result=Core::loadModel($this->name)->saveObject($data);
		if($result){
			return [RESULT_SUCCESS, '更新成功', url('Setting/site')];
		}else{
			return [RESULT_ERROR, '更新失败', url('Setting/site')];
		}
	}
	public function getSetting($data){
		return self::getOneObject($data);
	}


	//循环删除目录和文件函数  
	public function delDirAndFile($dirName)  {
		if ( $handle = opendir("$dirName") ) {
			while(false!==($item=readdir($handle))){
				if($item!="."&&$item!=".."){  
					if ( is_dir( "$dirName/$item" ) ) {  
						$this->delDirAndFile( "$dirName/$item" );  
					} else {  
						unlink("$dirName/$item");
					}  
			   }  
		   }  
		   closedir( $handle );  
		   rmdir($dirName);
		}
	}
}