<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\logic;
use \tpfcore\util\Tree;
use \tpfcore\util\Data;
use \tpfcore\Core;
/**
 *  导航逻辑
 */
class Push extends AdminBase
{
	private $arr=[];
	public function savePush($data){
		$last_id=Core::loadModel($this->name)->saveObject($data);
		if($last_id){
        	return [RESULT_SUCCESS, '操作成功', url('Push/index')];
        }
        return [RESULT_ERROR, '操作失败'];
	}

	public function getPushList($where = [], $field = true, $order = '', $is_paginate = true){
		$paginate_data = $is_paginate ? ['rows' => DB_LIST_ROWS] : false;
		return self::getList(["where"=>$where ,"field"=>$field,"order"=>$order, "paginate"=>$paginate_data]);
	}
}