<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\logic;
use \tpfcore\Core;

class Store extends AdminBase
{
	public function getStore($data){
		return self::getList($data);
	}
	public function delStore($data){
		$result=self::updateObject($data,["is_delete"=>1,"is_hot"=>0,"is_check"=>0]);
		if($result){
			return [0, '操作成功', url('Stores/index')];
		}else{
			return [-1, '操作失败', url('Stores/index')];
		}
	}
	public function recovery($data){
		$result=self::updateObject($data,["is_delete"=>0]);
		if($result){
			return [0, '操作成功', url('Stores/index')];
		}else{
			return [-1, '操作失败', url('Stores/index')];
		}
	}
	public function refuse($data){
		$result=self::updateObject(["id"=>$data['id']],["is_check"=>4,"reason"=>$data['reason']]);
		if($result){
			return [0, '操作成功', url('Stores/index')];
		}else{
			return [-1, '操作失败', url('Stores/index')];
		}
	}
}