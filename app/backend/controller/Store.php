<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\backend\controller;
use \tpfcore\Core;
class Store extends AdminBase
{
    public function index()
    {
        return $this->fetch("index",[
            'list'=>Core::loadModel("Store")->getStore(
                [
                    "paginate"  =>['rows' => DB_LIST_ROWS],
                    "field"=>"__STORE__.*,__USER__.id uid,__USER__.nickname",
                    "order"=>"id desc",
                    "join"=>['join' => "__USER__", 'condition' => "__USER__.id=__STORE__.user_id", 'type' => 'left'],
                ]
            )
        ]);
    }
    public function delStore(){
        $this->jump(Core::loadModel("Store")->delStore($this->param));
    }
    public function recovery(){
        $this->jump(Core::loadModel("Store")->recovery($this->param));
    }
    public function refuse(){
        $this->jump(Core::loadModel("Store")->refuse($this->param));   
    }
} 
