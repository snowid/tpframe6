<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\backend\controller;
use \tpfcore\Core;
class Push extends AdminBase
{
    public function index()
    {
        return $this->fetch("index",[
            'list'=>Core::loadModel($this->name)->getPushList()
        ]);
    }
    public function add(){
        IS_POST && $this->jump(Core::loadModel($this->name)->savePush($this->param));
        return $this->fetch("add"); 
    }
    public function config(){
        IS_POST && $this->jump(Core::loadModel($this->name)->savePush($this->param));
        return $this->fetch("config",[
            'list'=>Core::loadModel($this->name)->getPushList(['id'=>$this->param['id']]),
            'handle'=>isset($this->param['handle'])?$this->param['handle']:"alipay",
            'id'=>$this->param['id']
        ]);
    }
} 
