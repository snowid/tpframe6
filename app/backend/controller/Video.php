<?php
/**
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
 */
namespace app\backend\controller;
use \tpfcore\Core;
class Video extends AdminBase
{
    public function index(){
        return $this->fetch('index',[
            'list'=>Core::loadModel($this->name)->getVideo()
        ]);
    }
    public function add(){
        IS_POST && $this->jump(Core::loadModel($this->name)->saveVideo($this->param));
        return $this->fetch('add');
    }
    public function edit(){
        IS_POST && $this->jump(Core::loadModel($this->name)->saveVideo($this->param));
        return $this->fetch('add',[
            'id'=>$this->param['id'],
            'list'=>Core::loadModel($this->name)->getVideo(["where"=>$this->param])
        ]);
    }
    public function del(){
        $this->jump(Core::loadModel($this->name)->del($this->param));
    }
}
?>