<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\backend\model;

class Push extends AdminBase
{
    protected function setConfigAttr($value)
    {
        return json_encode($value);
    }
}
