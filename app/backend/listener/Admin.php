<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// | Site home: http://www.tpframe.com
// +----------------------------------------------------------------------
namespace app\backend\listener;

use think\facade\Db;
use tpfcore\Core;
use think\facade\Session;
use app\backend\event\Admin as adminEVent;

class Admin
{
    /**
     * 入口
     */
    public function handle(adminEVent $av)
    {
        $av->recordLog();
    }
}
