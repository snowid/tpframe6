<?php
namespace app\install\controller;
use app\common\controller\ControllerBase;
use \tpfcore\Core;
use think\facade\Request;
use think\facade\Session;
class Index extends ControllerBase {
    public static $db_connect=null;
    function _initialize(){
        parent::_initialize();
        if(@file_exists(root_path()."/data/install.lock")){
            $this->redirect(SITE_PATH."/");
        }
        if ('7.1.0' > phpversion()){
            header("Content-type:text/html;charset=utf-8");
            exit('您的php版本过低，不能安装本软件，请升级到PHP7.1+或更高版本再安装，谢谢！');
        }
    }
    //首页
    public function index() {
        Session::set("_install_step",1);
        
        return $this->fetch(":index");
    }
    public function step1() {
        $this->redirect(SITE_PATH."/install/");
    }

    public function step2(){
        Session::set("_install_step",2);
        try{
            if(file_exists(app()->getBasePath().'extra/database.php')){
                unlink(app()->getBasePath().'extra/database.php');
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }

        $data=array();
        $data['phpversion'] = @ phpversion();
        $data['os']=PHP_OS;
        $tmp = function_exists('gd_info') ? gd_info() : array();
        $server = $_SERVER["SERVER_SOFTWARE"];
        $host = (empty($_SERVER["SERVER_ADDR"]) ? ((!empty($_SERVER["SERVER_HOST"]))?:$_SERVER["SERVER_NAME"]) : $_SERVER["SERVER_ADDR"]);
        $name = $_SERVER["SERVER_NAME"];
        $max_execution_time = ini_get('max_execution_time');
        $allow_reference = (ini_get('allow_call_time_pass_reference') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
        $allow_url_fopen = (ini_get('allow_url_fopen') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
        $safe_mode = (ini_get('safe_mode') ? '<font color=red>[×]On</font>' : '<font color=green>[√]Off</font>');

        $err = 0;
        if (empty($tmp['GD Version'])) {
            $gd = '<font color=red>[×]Off</font>';
            $err++;
        } else {
            $gd = '<font color=green>[√]On</font> ' . $tmp['GD Version'];
        }

        if (class_exists('pdo')) {
            $data['pdo'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['pdo'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (extension_loaded('pdo_mysql')) {
            $data['pdo_mysql'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['pdo_mysql'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (extension_loaded('curl')) {
            $data['curl'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['curl'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (extension_loaded('gd')) {
            $data['gd'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['gd'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (extension_loaded('mbstring')) {
            $data['mbstring'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['mbstring'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (extension_loaded('fileinfo')) {
            $data['fileinfo'] = '<i class="fa fa-check correct"></i> 已开启';
        } else {
            $data['fileinfo'] = '<i class="fa fa-remove error"></i> 未开启';
            $err++;
        }

        if (ini_get('file_uploads')) {
            $data['upload_size'] = '<i class="fa fa-check correct"></i> ' . ini_get('upload_max_filesize');
        } else {
            $data['upload_size'] = '<i class="fa fa-remove error"></i> 禁止上传';
        }

        if (function_exists('session_start')) {
            $data['session'] = '<i class="fa fa-check correct"></i> 支持';
        } else {
            $data['session'] = '<i class="fa fa-remove error"></i> 不支持';
            $err++;
        }

        $folders = array(
            '../data',
            'data/uploads',
            '../app/extra'
        );
        $new_folders=array();
        foreach($folders as $dir){
            $Testdir = "./".$dir;
            $this->dir_create($Testdir);
            if(is_writable($Testdir)){
                $new_folders[$dir]['w']=true;
            }else{
                $new_folders[$dir]['w']=false;
                $err++;
            }
            if(is_readable($Testdir)){
                $new_folders[$dir]['r']=true;
            }else{
                $new_folders[$dir]['r']=false;
                $err++;
            }
        }
        $data['folders']=$new_folders;
        return $this->fetch(":step2",$data);
    }

    public function step3(){
        Session::set("_install_step",3);
        return $this->fetch(":step3");
    }

    public function step4(){
        return $this->fetch(":step4",[
            "data"=>json_encode($this->post)
        ]);
    }

    public function install(){
        if(IS_POST && isset($this->post['action'])){
            sleep(1);
            $action=$this->post['action'];
            switch ($action) {
                case 'database': 
                    $result=Core::loadModel("Database")->createDatabase($this->post);
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;
                
                case 'executeSql':
                    $result = Core::loadModel("Database")->executeSql(Session::get("db_connect"),$this->post);
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;

                case 'update_site_config':  
                    $result = Core::loadModel("Database")->update_site_config(Session::get("db_connect"),$this->post);
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;
                    
                case 'create_admin_account':  
                    $result = Core::loadModel("Database")->create_admin_account(Session::get("db_connect"),$this->post);
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;        

                case 'create_site_config':  
                    $result = Core::loadModel("Database")->create_site_config(Session::get("db_connect"),$this->post);
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;

                case 'create_config': 
                    $result = Core::loadModel("Database")->create_config(Session::get("db_connect"));
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;
                    
                case 'create_version':
                    Session::set("_install_step",4);
                    $result = Core::loadModel("Database")->create_version(Session::get("db_connect"));
                    return json(["code"=>$result[0],"msg"=>$result[1],"data"=>$result[2]]);
                    break;

                default:
                    $this->jump([-1,"安装出错",['status'=>'error']]);
                    break;
            }
        }else{
            $this->jump([-1,"非法安装程序！",['status'=>'error']]);
        }
    }

    public function step5(){
        if(Session::get("_install_step")==4){
            @touch(root_path().'data/install.lock');
            $this->curl_post("https://www.tpframe.com/taskmger/install",['domain'=>$_SERVER['HTTP_HOST'],'ip'=>request()->ip()]);
            Session::clear();
            return $this->fetch(":step5");
        }else{
            $this->error("非法安装！");
        }
    }

    public function checkpass(){
        if(IS_POST){
            $dbconfig=$this->post;
            $dbconfig['type']="mysql";
            return Core::loadModel("Database")->checkpass($dbconfig);
        }else{
            $this->jump([1,"post request"]);
        }
    }
    public function dir_create($path, $mode = 0777){
        if (is_dir($path)) return true;
        $ftp_enable = 0;
        $temp = explode('/', $path);
        $cur_dir = '';
        $max = count($temp) - 1;
        for ($i = 0; $i <= $max; $i++) {
            $cur_dir .= $temp[$i] . '/';
            if (@is_dir($cur_dir))
                continue;
            @mkdir($cur_dir, 0777, true);
            @chmod($cur_dir, 0777);
        }
        return true;
    }

    /**
     * 模拟post进行url请求
     * @param string $url
     * @param string $param
     */
    private function curl_post($url = '', $param = '') {
        if (empty($url) || empty($param)) {
            return false;
        }
        
        $postUrl = $url;
        $curlPost = $param;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        
        return $data;
    }
}

