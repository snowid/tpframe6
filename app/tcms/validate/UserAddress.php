<?php
namespace app\tcms\validate;
use think\Db;
class UserAddress extends FrontendBase
{
    // 验证规则
    protected $rule = [
        'consignee'             => 'require',
        'mobile'                => 'require|length:11,11|checkPhone',       // 密码长度6-18位
        'province'              => 'require|>:0',
        'city'                  => 'require|>:0',
        'address'               => 'require',
    ];


    // 应用场景
    protected $scene = [
        'add'             =>  ['consignee',"mobile",'province','city','address'],

    ];
    protected $message  =   [
        'consignee.require'             => '收货人不能为空',
        'mobile.require'                => '手机号必须',
        'mobile.length'                 => '手机号码11位',
        'province.require'              => '请选择地址',
        'province.gt'                   => '请选择地址',
        'city.require'                  => '请选择地址',
        'city.gt'                       => '请选择地址',
        'address.require'               => '详细地址不能为空',
    ];
    /*
        检查老密码是否正确
    */
    protected function checkPhone($value){
        if(!preg_match("/13[123569]{1}\d{8}|15[1235689]\d{8}|188\d{8}/", $value)){
            return "手机号码格式不正确";
        }
        return true;
    }
}