<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\model;
use think\Model;
use think\Request;

class User extends FrontendBase 
{
	protected $insert = ['type'=>0,'create_time'];
    protected $auto = ["last_login_time","last_login_ip"];
    protected function setPasswordAttr($value)
    {
        return '###'.md5($value.DATA_ENCRYPT_KEY);
    }
    protected function setLastLoginIpAttr($value){
        return Request::instance()->ip();
    }
    protected function setLastLoginTimeAttr($value){
        return time();
    }
    protected function setCreateTimeAttr($value){
        return time();
    }
    protected function setUsernameAttr($value){
        return trim($value);
    }
}
?>