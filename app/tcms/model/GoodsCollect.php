<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------
namespace app\tcms\model;
use think\Model;

class GoodsCollect extends FrontendBase 
{
	protected $insert = ['user_id','addtime'];

    protected function setUserIdAttr(){
        return session("tpf_login_sign")['id'];
    }
    protected function setAddtimeAttr($value){
        return time();
    }
}
?>