<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\tcms\controller;
use \tpfcore\Core;
use think\Request;
use tpfcore\helpers\Json;
class Auth extends FrontendBase
{
	public function _initialize(){
		parent::_initialize();
		if(!session("?tpf_login_sign")){
			$this->view->engine->layout(false);
			$request = Request::instance();
			$curr_url=$request->param('return_url')?$request->param('return_url'):$request->url();
			$return_url=str_replace(".html", "", $curr_url);
			$url=url("ucenter/User/login",[],false)."?return_url=".urlencode($return_url);
			if(IS_AJAX){
				die(Json::encode(["code"=>-1, "msg"=>"请先登录","url"=>$url]));
			}else{
				//die($this->fetch(THINK_PATH . 'tpl' . DS . 'public_jump.tpl',['title'=>'请先登录','url'=>$url]));
				header("Location:$url");exit;
			}
        }
	}
}