<?php 
return array (
  'DEFAULT_THEME' => 'tcms',
  'HTML_CACHE_ON' => '0',
  'ADMIN_LOG_SWITCH' => '0',
  'WEB_SITE_CLOSE' => '0',
  'ADMIN_LOGIN_LLIMIT_IP' => '',
  'ADMIN_LOGIN_VERIFY_SWITCH' => '1',
  'allow_upload_pic_type' => 'jpg,png,gif,bmp',
  'allow_upload_pic_size' => '2048',
  'upload_pic_position' => 'upyun',
  'mimi_program' => 
  array (
    'module' => 
    array (
      1 => 
      array (
        'title' => '案例展示',
        'base_url' => '../case_item/case_item',
        'more_url' => '../case_list/case_list',
        'web_more_url' => 'case-list.html',
        'web_base_url' => 'case-item.html',
        'style' => 1,
        'limit' => 4,
        'category_id' => 108,
        'sort' => 'datetime desc',
      ),
      2 => 
      array (
        'title' => '新闻中心',
        'base_url' => '../news_item/news_item',
        'more_url' => '../news_list/news_list',
        'web_more_url' => 'news-list.html',
        'web_base_url' => 'news-item.html',
        'style' => 1,
        'limit' => 4,
        'category_id' => 108,
        'sort' => 'datetime desc',
      ),
      3 => 
      array (
        'title' => '视频展示',
        'base_url' => '../video_list/video_list',
        'more_url' => '../video_list/video_list',
        'web_more_url' => 'video-list.html',
        'web_base_url' => 'video-list.html',
        'style' => 1,
        'limit' => 4,
        'category_id' => 108,
        'sort' => 'datetime desc',
      ),
      4 => 
      array (
        'title' => '相册展示',
        'base_url' => '../photo_item/photo_item',
        'more_url' => '../photo_list/photo_list',
        'web_more_url' => 'photo-list.html',
        'web_base_url' => 'photo-item.html',
        'style' => 1,
        'limit' => 4,
        'category_id' => 108,
        'sort' => 'datetime desc',
      ),
    ),
    'config' => 
    array (
      'url' => 'http://test.tpframe.com/cms/mini_program/chat',
      'token' => 'tpframe',
      'encodingAESKey' => 'Afeaga1611AWF6AG163F16G161AWF',
      'appid' => 'wx2w4f614646161456',
      'secret' => 'awgawfwawfawgawfawefwafewfa',
      'mini_api_validate' => 1,
      'api_time_limit' => 6,
      'apiKey' => '',
      'api_ip_authorize' => '',
    ),
    'setting' => 
    array (
      'mobile' => '18323008570',
      'qq' => '510974211',
      'email' => '510974211@qq.com',
      'address' => 
      array (
        'longitude' => '106.539397',
        'latitude' => '29.580562',
      ),
      'is_comment' => '1',
      'firstName' => '',
      'organization' => '',
      'init' => 
      array (
        'about_cid' => '12',
        'nav_style' => 1,
      ),
    ),
  ),
  'site_seo_title' => 'TPFrame管理框架',
  'site_seo_keywords' => 'TPFrame,php,管理框架,cms,简约风, simplewind,framework',
  'site_seo_description' => 'TPFrame是简约风网络科技发布的一款用于快速开发的内容管理框架',
  'WEB_REGISTER_CLOSE' => '0',
);