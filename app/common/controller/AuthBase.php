<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\common\controller;
use \tpfcore\Core;
/**
 * 认证基类
 * 须要登录后才行操作的操作器继承此类即可
 */
class AuthBase extends ControllerBase
{
    /**
     * 插件基类构造方法
     */
    public function _initialize()
    {
        parent::_initialize();
        /*
            用户认证
            token
        */
        $result=Core::loadModel("LogicBase")->checkLogin($this->param);
        $result && $this->jump($result);
    }
     
}
