<?php
// +----------------------------------------------------------------------
// | Author: yaoyihong <510974211@qq.com>
// +----------------------------------------------------------------------

namespace app\common\controller;
use think\Request;
use tpfcore\Core;
use think\facade\Config;
use tpfcore\helpers\StringHelper;
use think\facade\View;
/**
 * 插件控制器基类
 */
class AddonBase extends ControllerBase
{
    /**
     * 插件基类构造方法
     */
    public function _initialize()
    {
        parent::_initialize();

        // 检查模块是否已经安装

        $addon_list = \tpfcore\helpers\FileHelper::get_dir("../".ADDON_DIR_NAME);

        $out_action=["ajaxdata","addonInfo"];

        if(in_array(MODULE_NAME, $addon_list)&&!in_array(ACTION_NAME, $out_action)){

            $module_name=MODULE_NAME;

            if(!Core::loadModel("Addon","common","logic")->isInstall(['module'=>MODULE_NAME,'status'=>1])){

                $this->tip([-1,"请先安装或启用模块{$module_name}插件后再试",null]);
                
            }
        }
    }
    
    /**
     * 插件模板渲染
     */
    public function addonTemplate($template_name = '',$params=[])
    {
        
        $class = get_class($this);

        $arr=explode("\\", $class);

        $module = strtolower($arr[1]);
        
        $params = array_merge($params,['static_path'=>'/assets/' .ADDON_DIR_NAME . "/{$module}",'admin_assets_path'=>'/assets/backend/']);

        $view_path = app()->getRootPath().ADDON_DIR_NAME."/{$module}/view/";

        View::config(['view_path' => $view_path,'tpl_replace_string'=>['__THEMES__' =>  DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'backend']]);

        echo View::fetch($template_name,$params);
    }
    /*
        插件使用说明
    */
    public function doc(){

        echo "该插件开发者未完善使用文档";

    }
}
