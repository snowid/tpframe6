<?php
namespace app\common\middleware;

class AppMiddleware
{
    public function handle($request, \Closure $next)
    {
    	
        $response = $next($request);

        return $response;
    }
}