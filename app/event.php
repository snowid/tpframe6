<?php
// 事件定义文件
return [
    'bind'      => [
        'admin_log' => 'AdminLog'
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => ['app\common\listener\InitBase','app\common\listener\InitListener'],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'AdminLog' => ['app\backend\listener\Admin']
    ],

    'subscribe' => [
    ],
];
