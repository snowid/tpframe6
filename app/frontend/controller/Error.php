<?php
namespace app\frontend\controller;

use tpfcore\Core;
use think\Db;

class Error extends FrontendBase
{
    public function __call($method, $args)
    {
        $this->tip([0,"你访问的页面不存在",null]);
    }
}