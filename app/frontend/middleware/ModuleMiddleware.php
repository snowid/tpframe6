<?php
namespace app\frontend\middleware;

class ModuleMiddleware
{
    public function handle($request, \Closure $next)
    {

        $response = $next($request);

        return $response;
    }
}