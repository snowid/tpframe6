<?php
namespace app\frontend\middleware;

class Route
{
    public function handle($request, \Closure $next)
    {

        $response = $next($request);

        return $response;
    }
}