<?php
/**
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
 * ============================================================================
 * 版权所有 2017-2077 tpframe工作室，并保留所有权利。
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下对程序代码进行修改和使用；
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * 核心类
 */
namespace tpfcore;
use \think\Loader;
use tpfcore\helpers\StringHelper;
use think\exception\ClassNotFoundException;
use think\exception\HttpException;
use think\facade\Event;
class Core{
	/**
     * @var array 实例数组
     */
    protected static $instance = [];
	/**
    * 加载模型
    * @access protected
    * @param string   $name     模型名
    * @param string   $module   模块名
    * @param string   $layer    分层名
    * @return mixed
    */
	final static function loadModel($name = '', $module = '' , $layer='')
	{
	    $backtrace_array = debug_backtrace(false, 1);

	    $current_directory_name = basename(dirname($backtrace_array[0]['file']));

	    $module=$module?:app("http")->getName();

	    $layer_= [LAYER_CONTROLLER_NAME,LAYER_SERVICE_NAME,LAYER_LOGIC_NAME,LAYER_MODEL_NAME,LAYER_MODEL_NAME];

	    $return_object = null;

	    //$layer = $layer?$layer_[array_search($layer,$layer_)+1]:$layer_[array_search($current_directory_name,$layer_)+1];
	    $layer = $layer?:$layer_[array_search($current_directory_name,$layer_)+1];

	    $current_namespace = is_dir(ADDON_PATH.$module)?ADDON_DIR_NAME:"app";

	    $class = $current_namespace."\\".$module."\\".$layer."\\".$name;

	    if($layer == LAYER_SERVICE_NAME && !class_exists($class)){

    		$class = $current_namespace."\\".$module."\\".$layer_[array_search($layer,$layer_)+1]."\\".$name;

    		if(!class_exists($class)){
		
				$class='app'."\\".MODULE_COMMON_NAME."\\".$layer."\\".$name;

				if(!class_exists($class)){

					$class='app'."\\".MODULE_COMMON_NAME."\\".$layer_[array_search($layer,$layer_)+1]."\\".$name;

					$layer = $layer_[array_search($layer,$layer_)+1];

				}
			}else{

				$layer = $layer_[array_search($layer,$layer_)+1];

			}

	    }

	    if(!class_exists($class)){

	    	$module="common";

	    	$class='app'."\\".MODULE_COMMON_NAME."\\".$layer."\\".$name;
	    	
	    }


	    $key = $current_namespace.$module.$layer.$name;

	    if (isset(self::$instance[$key])) {

            return self::$instance[$key];

        }


        if(class_exists($class)){

			$return_object = $layer=="controller" ? new $class(app()):new $class();

			return self::$instance[$key] = $return_object;
        }else{

        	throw new HttpException(404, "class {$class} not exists");

        }

	}
	/**
     * 调用模块的操作方法 参数格式 [模块/控制器/]操作
     * @param string        $action 调用动作 [控制器/操作]
     * @param string|array  $vars 调用参数 支持字符串和数组
	 * @param string        $model 调用模块
     * @param string        $layer 要调用的控制层名称（logic\service\model\controller）
     * @param bool          $appendSuffix 是否添加类名后缀
     * @return mixed
     */
	final static function loadAction($action,$vars = [],$model="",$layer="",$appendSuffix = false){
		$name=explode("/", $action)[0];
		$common_service='app'."\\".MODULE_COMMON_NAME."\\".LAYER_SERVICE_NAME."\\".$name;
		$common_logic='app'."\\".MODULE_COMMON_NAME."\\".LAYER_LOGIC_NAME."\\".$name;
		$common_model='app'."\\".MODULE_COMMON_NAME."\\".LAYER_MODEL_NAME."\\".$name;

		$module_service='app'."\\".MODULE_NAME."\\".LAYER_SERVICE_NAME."\\".$name;
		$module_logic='app'."\\".MODULE_NAME."\\".LAYER_LOGIC_NAME."\\".$name;
		$module_model='app'."\\".MODULE_NAME."\\".LAYER_MODEL_NAME."\\".$name;
		$module_controller='app'."\\".MODULE_NAME."\\".LAYER_CONTROLLER_NAME."\\".$name;

		if(!$model){
			if($layer){
				switch ($layer){
					case LAYER_CONTROLLER_NAME : $result = class_exists($module_controller)?app()->action($action, $vars, LAYER_CONTROLLER_NAME, $appendSuffix):app()->action('common'."/".$action, $vars, LAYER_CONTROLLER_NAME, $appendSuffix); break;
			        case LAYER_SERVICE_NAME    : $result = class_exists($module_service)?app()->action($action, $vars, LAYER_SERVICE_NAME, $appendSuffix):app()->action('common'."/".$action, $vars, LAYER_SERVICE_NAME, $appendSuffix); break;
			        case LAYER_LOGIC_NAME      : $result = class_exists($module_logic)?app()->action($action, $vars, LAYER_LOGIC_NAME, $appendSuffix):app()->action('common'."/".$action, $vars, LAYER_LOGIC_NAME, $appendSuffix); break;
			        case LAYER_MODEL_NAME      : $result = class_exists($module_model)?app()->action($action, $vars, LAYER_MODEL_NAME, $appendSuffix):app()->action('common'."/".$action, $vars, LAYER_MODEL_NAME, $appendSuffix); break;
			        default                    : break;
				}
			}else{
				//echo $action;die;
				if(class_exists($common_logic)){
					$url='common'."/".$action;
					//echo $url;die;
					$result=app()->action($url, $vars, LAYER_LOGIC_NAME, $appendSuffix);
				}
				else if(class_exists($module_logic)){
					$result=app()->action($action, $vars, LAYER_LOGIC_NAME, $appendSuffix);
				}
				else if(class_exists($common_service)){
					$url='common'."/".$action;
					$result=app()->action($url, $vars, LAYER_SERVICE_NAME, $appendSuffix);
				}else{
					$result=app()->action($action, $vars, LAYER_SERVICE_NAME, $appendSuffix);
				}
			}
			
		}else{
			$module_service='app'."\\".$model."\\".LAYER_SERVICE_NAME."\\".$name;
			$module_logic='app'."\\".$model."\\".LAYER_LOGIC_NAME."\\".$name;
			$module_model='app'."\\".$model."\\".LAYER_MODEL_NAME."\\".$name;
			$module_controller='app'."\\".$model."\\".LAYER_CONTROLLER_NAME."\\".$name;
			$url=$model."/".$action;
			if($layer){
				switch ($layer){
					case LAYER_CONTROLLER_NAME : $result = app()->action($url, $vars, LAYER_CONTROLLER_NAME, $appendSuffix);break;
			        case LAYER_SERVICE_NAME    : $result = app()->action($url, $vars, LAYER_SERVICE_NAME, $appendSuffix);break;
			        case LAYER_LOGIC_NAME      : $result = app()->action($url, $vars, LAYER_LOGIC_NAME, $appendSuffix);break;
			        case LAYER_MODEL_NAME      : $result = app()->action($url, $vars, LAYER_MODEL_NAME, $appendSuffix);break;
			        default                    : break;
				}
			}else{
				if(class_exists($module_logic)){
					$result=app()->action($url, $vars, LAYER_LOGIC_NAME, $appendSuffix);
				}else{
					$result=app()->action($url, $vars, LAYER_SERVICE_NAME, $appendSuffix);
				}

			}
		}
		return $result;
	}
	/**
	 * 获取插件类的类名
	 * @param strng $name 插件名
	 */
	final static function get_addon_class($catename = '', $name = '')
	{

	    $addonClass=StringHelper::s_format_class($catename);

	    $class = ADDON_DIR_NAME."\\{$catename}\\{$addonClass}";
	    
	    return $class;
	}

	/**
	 * 钩子
	 */
	final static function hook($tag = '', $params = [])
	{
	    
	    Event::trigger($tag, $params);
	}
	/**
	 * 插件显示内容里生成访问插件的url
	 * @param string $url url  插件名类名://控制器/方法  eg:FriendLink://FriendLink/index
	 * @param array $param 参数
	 * @author <510974211@qq.com>
	 */
	final static function addons_url($url, $param = array())
	{

	    $url        =  parse_url($url);   // login://qq/login ====> Array ( [scheme] => login [host] => qq [path] => /login )

	    if(!isset($url['scheme']) || !isset($url['host'])){

	    	return null;

	    }

	    $action     =  isset($url['path'])?$url['path']:"/index";

	    $module = StringHelper::s_format_underline($url['scheme']);

	    $controller = StringHelper::s_format_underline($url['host']);

	    $action = strtolower(substr($action, 1));

	    return url("$module/$controller/$action", $param);
	}
	
	
	/**
	 * 获取验证模型
	 * @param var $param 参数
	 * @author <510974211@qq.com>
	 */
	final static function loadValidate($name){
		if(null==$name || empty($name) || ""==$name){
			return null;
		}

		$backtrace_array = debug_backtrace(false, 1);

		$addon_path=str_replace(ROOT_PATH, "\\", dirname(dirname($backtrace_array[0]['file'])));

		$addon_path=str_replace(DS, "\\", $addon_path);

		$validateModel=$addon_path."\\validate\\".$name;

		$validateModel = class_exists($validateModel)?$validateModel:"app\\common"."\\validate\\".$name;

		try {

			return new $validateModel();

		} catch (Exception $e) {

			throw new HttpException(404, 'class not exists:' . $e->getClass());
			
		}
		
	}
	/*
	* 插件里面的数据验证
	* name 插件名字
	*/
	final static function addonValidate($name){
		if(null==$name || empty($name) || ""==$name){
			return null;
		}

		$backtrace_array = debug_backtrace(false, 1);

		$addon_path=str_replace(ROOT_PATH, "\\", dirname(dirname($backtrace_array[0]['file'])));

		$addon_path=str_replace(DS, "\\", $addon_path);

		$validateModel=$addon_path."\\validate\\".$name;
		
		if(class_exists($validateModel)){
			
			return new $validateModel;

		}
		return null;

	}
}
?>