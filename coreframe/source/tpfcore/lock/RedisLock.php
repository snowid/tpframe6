<?php
/** 
 * php锁机制
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * @author 510974211@qq.com
    $redis = new RedisLock('127.0.0.1', 6379, 0);
    $redis->getlock('lock');
    .....
    $redis->releaseLock('lock');
 */
namespace tpfcore\lock;
use tpfcore\base\ILock;
class RedisLock implements ILock
{
    private $_redis=null;

    public function __construct($config=null){

        $this->_redis = $this->_redis == null ? new redis() : $this->_redis;

        $this->_redis->connect($config);
    }

    public function getlock($key, $expire = 60)
    {
        if(!$key) {
            return false;
        }
        do {
            /*
            if($redis->set("Lock:{$key}",time(),['NX','EX'=>10])){       //   Redis 2.6.12 版本开始， SET 命令的行为可以通过一系列参数来修改  NX 仅在键不存在时设置键。 XX - 只有在键已存在时才设置。
            */
            if($acquired = ($this->_redis->setnx("Lock:{$key}", time()))) { // 如果redis不存在，则成功
                $this->_redis->expire($key, $expire);   // 锁的过期时间
                break;
            }
            usleep($expire);

        } while (true);

        return true;
    }

    //释放锁
    public function releaseLock($key)
    {
        $this->_redis->del("Lock:{$key}");
        $this->_redis->close();
    }
}